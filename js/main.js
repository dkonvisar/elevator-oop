$(document).ready(function () {
    var was_called = false;
    $('#calling').on('submit', function () {
        // console.log($(this).serialize());
        $.ajax({
            type: "POST",
            url: 'includes/calling-handler.php',
            data: $(this).serialize(),
        })
            .done(function (response) {
                was_called = true;
                // console.log(response);
                $('.statuses').append(response);
            })
        return false;
    })

    $('#target').on('submit', function () {
        // console.log($(this).serialize());
        $.ajax({
            type: "POST",
            url: 'includes/target-handler.php',
            data: $(this).serialize(),
        })
            .done(function (response) {
                // console.log(response);
                if (was_called) {
                    $('.statuses').append(response);
                } else {
                    $('.statuses').append("<p class='message-warning'>Firstly, You should call the elevator!</p>");
                }
            })
        return false;
    })

    $("#calling").submit(function () {
        var count = $(this).data("count") || 1;
        if (count % 3 === 0) {
            // console.log('broken');
            $.ajax({
                type: 'POST',
                url: 'includes/broken-handler.php',
                data: 'broken=true'
            })
                .done(function (response) {
                    // console.log(response);
                    $('.statuses').append("<p class='message-broken'>" + response + "</p>");
                })
        }
        $(this).data("count", ++count);
    });

    $("#target").submit(function () {
        var count = $(this).data("count") || 1;
        if (count % 3 === 0) {
            // console.log('broken');
            $.ajax({
                type: 'POST',
                url: 'includes/broken-handler.php',
                data: 'broken=true'
            })
                .done(function (response) {
                    // console.log(response)
                    $('.statuses').append("<p class='message-broken'>" + response + "</p>");
                })
        }
        $(this).data("count", ++count);
    });
})