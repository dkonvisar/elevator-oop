<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Elevator</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<main>
    <section class="content">
        <div class="left-column">
            <ul>
                <li><span>9</span></li>
                <li><span>8</span></li>
                <li><span>7</span></li>
                <li><span>6</span></li>
                <li><span>5</span></li>
                <li><span>4</span></li>
                <li><span>3</span></li>
                <li><span>2</span></li>
                <li><span>1</span></li>
            </ul>
            <p class="info"></p>
        </div>
        <div class="right-column">
            <form id="calling">
                <label for="calling-select">Elevator is called on the floor:</label>
                <select id="calling-select" class="calling-select" name="calling-select" required>
                    <option value="">Choose</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                </select>
                <button class="calling-button" type="submit">Call a Lift</button>
            </form>
            <form id="target">
                <label for="target-select">Elevator should move to the floor:</label>
                <select id="target-select" class="target-select" name="target-select" required>
                    <option value="">Choose</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                </select>
                <button class="target-button" type="submit">Go to the floor</button>
            </form>
        </div>
    </section>
    <section class="messages">
        <div class="statuses"></div>
    </section>
</main>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="js/main.js"></script>
</body>
</html>