<?php
include_once '../classes/Elevator.php';
include_once '../classes/Passenger.php';

$target_to = $_POST['target-select'];

$elevator = new Elevator();
$elevator->moveToTargetFloor($target_to);
$elevator->openDoors();
$passenger = new Passenger();
$passenger->passengerComeOut();