<?php
include_once '../classes/Elevator.php';
include_once '../classes/Passenger.php';

$calling_from = $_POST['calling-select'];

$elevator = new Elevator();
$elevator->moveToCallingFloor($calling_from);
$elevator->openDoors();
$passenger = new Passenger();
$passenger->passengerComeIn();
