<?php

interface ElevatorInterface
{
    public function moveToCallingFloor($callingFloor);
    public function moveToTargetFloor($targetFloor);
    public function elevatorBreak($brokenValue);
}