<?php

abstract class AbstractElevator
{
    abstract public function openDoors();
}