<?php
include_once 'AbstractElevator.php';
include_once '../interfaces/ElevatorInterface.php';

class Elevator extends AbstractElevator implements ElevatorInterface
{
    public int $callingFloor;
    public int $targetFloor;
    public bool $brokenValue;

    /**
     * @param $callingFloor
     * @return void
     */
    public function moveToCallingFloor($callingFloor)
    {
        $this->callingFloor = $callingFloor;
        echo "Someone's called elevator on the " . $this->callingFloor . " floor" . "<br>";
    }

    /**
     * @param $targetFloor
     * @return void
     */
    public function moveToTargetFloor($targetFloor)
    {
        $this->targetFloor = $targetFloor;
        echo "Elevator is coming to the " . $this->targetFloor . " floor..." . "<br>";
    }

    /**
     * @param $brokenValue
     * @return string
     */
    public function elevatorBreak($brokenValue): string
    {
        $this->brokenValue = $brokenValue;
        return "Oops..You have broken the elevator. Please refresh the page or proceed and we maybe fix it :)";
    }

    /**
     * @return void
     */
    public function openDoors()
    {
        echo "<p class='message-service'>Doors are opening...</p>";
    }

    /**
     * @return void
     */
    public function __destruct()
    {
        echo "<p class='message-service'>Doors are closing...</p>";
    }
}
