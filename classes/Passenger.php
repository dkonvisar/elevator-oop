<?php

class Passenger
{
    /**
     * @return void
     */
    public function passengerComeIn()
    {
        echo "<p class='message-passenger'>Passenger's come inside cabin</p>";
    }

    /**
     * @return void
     */
    public function passengerComeOut()
    {
        echo "<p class='message-passenger'>Passenger's come outside cabin</p>";
    }
}
